# README #

This is a small project covering some interview algorithms asked during jobs interview.
Project Gradle compatible + tests

Topics covered are:

 - BitManipulation

 - HeapSort

 - LinkedList

 - MergeSort

 - Node

 - QuickSort

 - How to rotate a matrix

 - SleepSort (this is just for fun)

 - String Compression

Code is totally unreadable if you don't know already what the algorithm is doing, set to public because maybe one day I'll be less lazy and I'll put some comments and it might be useful to somebody

### What is this repository for? ###

Students,
 
Academic research,

Job interview preparation,




Pull requests are welcome :)


License
-------

    Copyright (C) 2015 Simone Arpe
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.