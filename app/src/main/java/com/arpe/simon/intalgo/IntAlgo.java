package com.arpe.simon.intalgo;

import java.io.IOException;
import java.util.Random;

/**
 * Created by arpes on 05/05/2015.
 */
public class IntAlgo {

    private static Random random = new Random();
    public static void main(String[] args) throws IOException {
        System.out.println("Hello World!");

        String s = "aaaaabbbbbcccccd";
        String res = StringCompression.compress(s);
        print(res);

//        int a = 7;
//        int b = 10;
//        print(a);
//        print(b);
//
//        print("swapping...");
//        a = a ^ b;
//        b = a ^ b;
//        a = a ^ b;
//
//        print(a);
//        print(b);

//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        System.out.println("Enter first number:");
//        int n1 = Integer.parseInt(br.readLine());


//        int[] array = new int[] {10,9,8,7,6,5,4,3,2,1,11};
//        printArray(array);
//
//        HeapSort.sort(array);
//        printArray(array);

//        int[] array2 = new int[] {10,11,9,8,7,11,12,33,43,34,34,34,4,32,233,34,5,567,67,7};
//        HeapSort.sort(array2);
//        printArray(array2);

        //100000
//        int right = ((1 << 5) - 1);
//        print(right);
//        String binary = Integer.toString(right, 2);
//        System.out.println(binary);

//        printArray(array2);
//        int[] result = MergeSort.sort(array2);
//        printArray(result);

//        int[][] matrix = new int[5][5];
//        int i,k;
//        for (i = 0;  i < matrix.length; i++) {
//            for (k = 0; k < matrix[i].length; k ++) {
//                matrix[i][k] = rand();
//            }
//
//        }
//
//        printMatrix(matrix);
//        RotateMatrix.rotate90(matrix);
//        printMatrix(matrix);

//        LinkedList l = new LinkedList();
//        l.add(1);
//        l.add(5);
//        l.add(6);
//        l.print();

//        Node n = new Node(7);
//        n.add(3);
//        Node del1 = n.add(2);
//        Node startLoop = n.add(9);
//
//        n.add(1);
//        n.add(2);
//        n.add(3);
//        n.add(4);
//        n.add(5);
//        n.add(6);
//        Node rec = n.add(7);
//        rec.setNext(startLoop);
//
//        Node result = checkStartingLoop(n);
//        print(result.getData());

//        n.print();


//        delete(del1);

        
//int res = BitManipulation.merge(0b1010101010, 1111, 7, 4);
//        String resBin = Integer.toString(res, 2);
//        print(resBin);
//
//        int[] array = new int[] {2,1,4,3,6,7};
//        SleepSort.sort(array);

    }

    public static boolean delete(Node n) {
    if (n == null || n.getNext() == null ) {
//        failed
        return false;
    }

        Node next = n.getNext();
        n.data = next.getData();
        n.next = next.next;
        return true;
    }

    public static Node checkStartingLoop(Node n) {
        Node fast = n;
        Node slow = n;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow == fast) {
                break;
            }
        }

        if (fast == null || fast.next == null) {
            return null;
        }

        slow = n;
        while (slow != fast) {
            slow = slow.next;
            fast = fast.next;
        }

        return fast;
    }

    public static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(String.format("%4d", i));
        }
        System.out.println();
    }

    private static void printMatrix(int[][] m) {
        System.out.println();
        for (int[] v : m) {
            printArray(v);
        }
        System.out.println();
    }

    private static int rand() {
        return random.nextInt(10);
    }

    public static void print(int i) {
        System.out.println(i);
    }

    public static void print(String i) {
        System.out.println(i);
    }


}
