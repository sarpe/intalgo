package com.arpe.simon.intalgo;

/**
 * Created by arpes on 07/05/2015.
 */
public class Node {

    public int data;
    public Node next;

    public Node(int i) {
        this.data = i;
    }

    public int getData() {
        return data;
    }


    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node add(int i) {
        Node end = new Node(i);
        Node n = this;
        while(n.next != null) {
            n = n.next;
        }
        n.next = end;
        return end;
    }

    public void print() {

        Node n = this;
        System.out.println(n.data);
        while (n.next != null) {
            System.out.println(n.next.getData());
            n = n.next;
        }

        System.out.println();
    }

}
