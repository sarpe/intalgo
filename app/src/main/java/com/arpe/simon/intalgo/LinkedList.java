package com.arpe.simon.intalgo;

/**
 * Created by arpes on 07/05/2015.
 */
public class LinkedList {

    private Node root;

    public LinkedList() {
        root = null;
    }

    public void add(int i) {
        if (root == null) {
            root = new Node(i);
            return;
        }
        Node temp = root;
        while(temp.getNext() != null) {
            temp = temp.getNext();
        }
        temp.add(i);
    }

    public void print() {
        if (root == null) {
            return;
        }
        Node temp = root;
        System.out.println();
        while (temp != null) {
            System.out.println(temp.getData());
            temp = temp.getNext();
        }
    }


}
