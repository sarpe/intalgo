package com.arpe.simon.intalgo;

import java.util.concurrent.CountDownLatch;

/**
 * Created by arpes on 12/05/2015.
 */
public class SleepSort {

    public static void sort(int[] array) {
        final CountDownLatch countDouwn = new CountDownLatch(array.length);
        for (final int i : array) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    countDouwn.countDown();
                    try {
                        countDouwn.await();
                        Thread.sleep(500 * i);
                        System.out.println(i);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }
    }
}
