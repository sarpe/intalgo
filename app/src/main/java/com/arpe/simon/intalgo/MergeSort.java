package com.arpe.simon.intalgo;

/**
 * Created by arpes on 06/05/2015.
 */
public class MergeSort {

    public static int[] sort(int[] a) {
        int[] tmp = new int[a.length];
        mergeSort(a, tmp, 0, a.length - 1);
        return tmp;
    }


    private static void mergeSort(int[] a, int[] tmp, int left, int right) {
        if( left < right ) {
            int center = (left + right) / 2;
            mergeSort(a, tmp, left, center);
            mergeSort(a, tmp, center + 1, right);
            merge(a, tmp, left, center + 1, right);
        }
    }


    private static void merge(int[] a, int[] tmp, int left, int right, int rightEnd ) {
        int leftEnd = right - 1;
        int k = left;
        int length = rightEnd - left + 1;

        while(left <= leftEnd && right <= rightEnd) {
            if (a[left] <= (a[right])) {
                tmp[k++] = a[left++];
            } else {
                tmp[k++] = a[right++];
            }
        }

        // Copy rest of first half
        while(left <= leftEnd) {
            tmp[k++] = a[left++];
        }
        // Copy rest of right half
        while(right <= rightEnd) {
            tmp[k++] = a[right++];
        }

        // Copy tmp back
        for(int i = 0; i < length; i++) {
            a[rightEnd] = tmp[rightEnd];
            rightEnd--;
        }
    }
}
