package com.arpe.simon.intalgo;

/**
 * Created by arpes on 11/05/2015.
 */
public class BitManipulation {

    public static int merge(int a, int b, int start, int end) {

        //aaaaaaaaaaaaaaaaaaaaa AND
        //111111110000000111111





        //aaaaaaaa0000000aaaaaa OR
        //        bbbbbbb000000

        //aaaaaaaabbbbbbbaaaaaa

        //create a mask

        //11111111 11111111 11111111 11111111

        int allOnes = ~0;
        int left = allOnes << start + 1; //1111111111100000000000
        print("left");
        printBin(left);

        int right = (1 << end) - 1;      //11111
        print("right");
        printBin(right);

        int mask = left | right; // 1111100000011111
        print("mask");
        printBin(mask);

        printBin(a);
        a = a & mask;
        printBin(a);

        b = b << end;
        a = a | b;

        return a;
    }

    public static void print(String i) {
        System.out.println(i);
    }
    public static void printBin(int i) {
        System.out.println(Integer.toString(i, 2));
    }
}
