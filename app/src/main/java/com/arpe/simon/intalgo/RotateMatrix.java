package com.arpe.simon.intalgo;

/**
 * Created by arpes on 07/05/2015.
 */
public class RotateMatrix {

    /*

    t 0 0 0 2
    0 0 0 0 0
    0 0 0 0 0
    0 0 0 0 0
    3 0 0 0 x

     */

    public static void rotate90(int[][] matrix) {
        int layer;
        int length = matrix.length;
        for (layer = 0; layer < length/2; layer++) {
            int layerLength = length - (layer * 2) - 1;
            int count;
            for(count = 0; count < layerLength; count ++) {
            int temp = matrix[layer + count][layer]; //1-1
            matrix[layer + count][layer] = matrix[layer + layerLength][layer + count];
            matrix[layer + layerLength][layer + count] = matrix[layer + layerLength - count][layer + layerLength];
            matrix[layer + layerLength - count][layer + layerLength] = matrix[layer][layer + layerLength - count];
            matrix[layer][layer + layerLength - count] = temp;

            }
        }

    }
}
