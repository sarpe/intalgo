package com.arpe.simon.intalgo;

import java.util.Stack;

/**
 * Created by arpes on 05/05/2015.
 */
public class QuickSort {

    public static void sortIterative(int[] array) {

        if (array == null || array.length == 0) {
            return;
        }

        Stack<Integer> stack = new Stack<>();
        stack.push(0); //left
        stack.push(array.length - 1); //right
        while (!stack.isEmpty()) {
            int right = stack.pop();
            int left = stack.pop();
            if (right > left) {
                int i = partition(array, left, right);
                stack.push(left);
                stack.push(i-1);
                stack.push(i+1);
                stack.push(right);
            }
        }

    }

    // partition a[left] to a[right], assumes left < right
    private static int partition(int[] a, int left, int right) {
        // DK:  added check if (left == right):
        if (left == right) {
            return left;
        }
        int i = left;
        int j = right;
        while (true) {
            // find item on left to swap
            while (a[i] < a[right]) {
                i++;
            }
            // a[right] acts as sentinel
            // find item on right to swap
            while (a[right] < a[--j]) {
                // don't go out-of-bounds
                if (j == left) {
                    break;
                }
            }
            // check if pointers cross
            if (i >= j) {
                break;
                }
            // swap two elements into place
            swap(a, i, j);
        }
        // swap with partition element
        swap(a, i, right);
        return i;
    }



    public static void sort(int[] array) {
        if (array == null || array.length == 0) {
            return;
        }
        sort(array, 0, array.length - 1);
    }

    private static void sort(int[] array, int start, int end) {

        if (start < end) {
            int k,j;
            k = start;
            j = end;
            boolean flag = false;

            while (k < j) {
                if (array[k] <= array[end]) {
                    k++;
                } else {
                    j--;
                    swap(array, k, j);
                    flag = true;
                }
            }

            if (flag) {
                swap(array, j, end);
            }

            sort(array, start, j - 1);
            sort(array, j + 1, end);
        }

    }

    private static void swap(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
