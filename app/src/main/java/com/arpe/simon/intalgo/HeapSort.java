package com.arpe.simon.intalgo;

/**
 * Created by arpes on 06/05/2015.
 */
public class HeapSort {

    public static void sort(int arr[]) {
        heapify(arr);

        int length = arr.length - 1;
        for (int i = length; i > 0; i--) {
            swap(arr, 0, i);
            maxHeap(arr, 0, length--);
        }
    }

    /* Function to build a heap */
    public static void heapify(int arr[]) {
        int length = arr.length - 1;
        for (int i = length / 2; i >= 0; i--) {
            maxHeap(arr, i, length);
        }
    }

    /* Function to swap largest element in heap */
    private static void maxHeap(int arr[], int i, int length) {
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        int max = i;
        if (left < length && arr[left] > arr[max]) {
            max = left;
        }

        if (right < length && arr[right] > arr[max]) {
            max = right;
        }

        if (max != i) {
            swap(arr, i, max);
            maxHeap(arr, max, length);
        }
    }

    /* Function to swap two numbers in an array */
    private static void swap(int arr[], int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

}
