package com.arpe.simon.intalgo;

/**
 * Created by arpes on 12/05/2015.
 */
public class StringCompression {
    public static String compress(String string) {
        int length = string.length();
        StringBuilder sb = new StringBuilder();
        char[] array = string.toCharArray();
        int i,j;
        for (i=0; i < length;) {
            int count = 1;
            char c = array[i];
            for(j=1;i+j < length && array[i+j] == c ;j++) {
                    count++;
            }

                sb.append(String.valueOf(c)+ count);
                i += j;

        }
        if (sb.length() > length) {
            return string;
        }
        return sb.toString();
    }
}
