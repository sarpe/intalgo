import com.arpe.simon.intalgo.MergeSort;
import com.arpe.simon.intalgo.QuickSort;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 12/05/2015.
 */
public class TestQuickSort {

    @Test
    public void quickSortTest() {
        int[] array = new int[] {7,77,700,1,2,3,4};
        QuickSort.sort(array);
        Assert.assertArrayEquals(array, new int[]{1,2,3,4,7,77,700});

    }
}
