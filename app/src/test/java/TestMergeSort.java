import com.arpe.simon.intalgo.MergeSort;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by arpes on 12/05/2015.
 */
public class TestMergeSort {

    @Test
    public void mergeSortTest() {
        int[] array = new int[] {7,77,700,1,2,3,4};
        int[] result = MergeSort.sort(array);
        Assert.assertArrayEquals(result, new int[]{1,2,3,4,7,77,700});

    }
}
